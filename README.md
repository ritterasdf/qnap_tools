# qnap_tools
This is a small repository of scripts, tools and guides (moslty hacks) to make life easier when working with QNAP NAS. Unless indicated otherwise files are written for the recent TS-653B NAS systems.

## custom_login.sh
A small shell script, which allows changing sshd options by modifying /etc/init.d/login.sh. It's usually run from autorun.sh, so on every boot, login.sh is manipulated.
This script was originally written as a quick hack to allow public key authentication but it can also be used for other purposes such as enabling additional users or switching on verbose sshd logging.



## curious sidenotes:
1. When looking at the sshd implementation for custom_login.sh, I found that when public key authorization is switched on, the server checks for `.ssh/authorized_keys` and `./ssh/authorized_keys2`